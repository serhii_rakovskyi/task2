package com.example.serhii_rakovskyi.task2;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private RecyclerView list;
    private Adapter adapter;
    private Context context = this;

    private List<myItem> items;


    LinearLayoutManager llm = new LinearLayoutManager(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initList();

    }



    private void initList() {
        items = new ArrayList<>();
        items.add(new myItem("Serhii", "23"));
        items.add(new myItem("Eugen", "24"));
        items.add(new myItem("Anton", "30"));
        items.add(new myItem("Michailo", "25"));
        items.add(new myItem("not me", "99"));
        items.add(new myItem("Serhii", "23"));
        items.add(new myItem("Eugen", "24"));
        items.add(new myItem("Anton", "30"));
        items.add(new myItem("Michailo", "25"));
        items.add(new myItem("not me", "99"));
        items.add(new myItem("Serhii", "23"));
        items.add(new myItem("Eugen", "24"));
        items.add(new myItem("Anton", "30"));
        items.add(new myItem("Michailo", "25"));
        items.add(new myItem("not me", "99"));


        list = (RecyclerView) findViewById(R.id.list);
        list.setLayoutManager(llm);

        adapter = new Adapter(items, this, new OnItemClickListener() {
            @Override
            public void onItemClick(myItem item) {
                Intent i = new Intent(context, deteils.class);
                i.putExtra("name", item.name);
                i.putExtra("age", item.age);
                startActivity(i);

            }
        });

        list.setAdapter(adapter);


    }


    public interface OnItemClickListener {
        void onItemClick(myItem item);
    }
}
