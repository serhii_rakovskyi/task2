package com.example.serhii_rakovskyi.task2;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

public class Adapter extends RecyclerView.Adapter<Adapter.ViewHolder> {

    List<myItem> items;
    Context context;
    private final MainActivity.OnItemClickListener listener;

    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView age;
        private TextView name;
        private Button button;

        public ViewHolder(View itemView) {
            super(itemView);
            this.age = itemView.findViewById(R.id.age);
            this.name = itemView.findViewById(R.id.name);
            this.button = itemView.findViewById(R.id.button);
        }
        public void bind(final myItem item, final MainActivity.OnItemClickListener listener) {

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    listener.onItemClick(item);
                }
            });
        }
    }


    @Override
    public Adapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        View v = LayoutInflater.from((viewGroup.getContext())).inflate(R.layout.item, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(Adapter.ViewHolder viewHolder, final int i) {
        viewHolder.age.setText(items.get(i).age);
        viewHolder.name.setText(items.get(i).name);
        viewHolder.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (items.get(i).name.equals("Serhii")) {
                    Toast.makeText(context, "It`s me", Toast.LENGTH_SHORT).show();
                }else
                    Toast.makeText(context, "It`s not me", Toast.LENGTH_SHORT).show();
            }
        });
        viewHolder.bind(items.get(i), listener);

    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    Adapter(List<myItem> items, Context context, MainActivity.OnItemClickListener listener) {
        this.items = items;
        this.context = context;
        this.listener = listener;
    }

}
